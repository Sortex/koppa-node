# Koppa

This package **Koppa** is a Node version of **Koppa.js**!
**Koppa.js** is a client-side rendering framework for single page applications which gets along without virtual DOM.

## Overview
Here is a small overview what Koppa.js offers.
1. Pages, Components (single File Templates)
2. JS-Module (extend the functionality)
3. Router
4. templating
5. live data

and many more...

## Installing
Create a folder for your new project and go inside.
Then open the console or the terminal and start your new node project.

```console
npm init
```
Answer the steps.

After the project is initialized you now install **koppa**.

```console
npm i @sortex/koppa
```

## Getting Started
If you want to start with a predefined project structure.

```console
npx koppa-startup
```

Now your project only needs the entry point, you create an **index.js** file in the root of your project. Then open the **package.json** file and change under **main** the specification to **index.js** if it is different.

Finally write this to the **index.js** file and save it.

```js
const Koppa = require('@sortex/koppa');

Koppa(8080);
```

**8080** is the port and can also be chosen in any other.
