#!/usr/bin/env node
const fs = require("fs-extra");

const [,, ...args] = process.argv; // args of commandline

let source = `${__dirname}/../../libs/startup`
let destination = `${process.cwd()}/src`

fs.copy(source, destination, function (err) {
    if (err){
        console.log('An error occured while copying the folder.')
        return console.error(err)
    }
    console.log('Copy completed!')
});
