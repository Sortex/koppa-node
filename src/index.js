let http = require("http"),
    url = require("url"),
    path = require("path"),
    fs = require("fs");

module.exports = async function koppa(port) {
  port = process.argv[2] || port;

  let generateDefaultFile = file => {
    // TODO replace base tag and add module tag
    return file;
  };

  http.createServer(function(request, response) {
    let defaultFile = path.basename('./src/index.html');
    let fileDir = path.dirname('./src/index.html');
    let fileUri = fileDir + url.parse(request.url).pathname;
    let fileSysPath = path.join(process.cwd(), fileUri);

    let fileName = path.basename(fileUri);

    if(fileName === 'koppa.js') {
      fileSysPath = path.join(process.cwd(), 'node_modules', '@sortex', 'koppa', 'dist', fileName);
    }

    let contentTypesByExtension = {
      '.html': "text/html",
      '.css':  "text/css",
      '.js':   "text/javascript"
    };

    fs.exists(fileSysPath, function(exists) {
      if(!exists) {
        response.writeHead(404, {"Content-Type": "text/plain"});
        response.write("404 Not Found\n");
        response.end();
        return;
      }

      if (fs.statSync(fileSysPath).isDirectory()) {
        fileName = defaultFile;
        fileSysPath += fileName;
      }

      fs.readFile(fileSysPath, "binary", function(err, file) {
        if(err) {
          response.writeHead(500, {"Content-Type": "text/plain"});
          response.write(err + "\n");
          response.end();
          return;
        }

        let headers = {};
        let contentType = contentTypesByExtension[path.extname(fileSysPath)];
        if (contentType) headers["Content-Type"] = contentType;
        response.writeHead(200, headers);

        response.write(file, "binary");

        response.end();
      });
    });
  }).listen(parseInt(port, 10));

  console.log("Koppa is running at\n  => http://localhost:" + port + "/\nCTRL + C to shutdown");
}
