let fs = require("fs");

let fileRead = filePath => {
  let content;
  try {
    content = fs.readFileSync(filePath, 'utf8');
  } catch (err) {
    console.error(err)
  }
  return content;
}

let getImports = strg => {
  let importList = [];
  strg = strg.match(/^[^(]*export default/g);
  let importStrings = `${strg[0]}`.match(/import\s+?(?:(?:(?:[\w*\s{},]*)\s+from\s+?)|)(?:(?:".*?")|(?:'.*?'))[\s]*?(?:;|$|)/g);

  for(let s of importStrings) {
    if(!s.includes('import Settings')) {
      importList.push(s.replace(/^import /, '').replace(/';$/, '').split(" from '"));
    }
  }

  return importList;
}

let getExport = strg => {
  return strg.replace(/^[^(]*export default /, '');
}

module.exports = (function() {
  let content = [];
  let indexContent = fileRead('./libs/koppa/index.js');
  let importList = getImports(indexContent);
  let settedImports = '';

  content.push(`import Settings from './settings.js';`);

  for(let imp of importList) {
    let fileContent = fileRead(`./libs/koppa${imp[1].substring(1, imp[1].length)}`);
    content.push(`let ${imp[0]} = ${getExport(fileContent)};`);
  }

  content.push(`export default ${getExport(indexContent)};`);

  fs.writeFile('./dist/koppa.js', content.join('\n'), 'utf-8', function (err) {
    if(err) {
      return console.log(err);
    }
    console.log('build file "koppa.js" is successful');
  });
})()
