import Settings from './settings.js';
let PrototypeExtensions = function(that) {
  function Nullable() {};

  Object.defineProperties(Object.prototype, {
    'exists': {
      get: function () {
          let size = 0, key;
          for(key in this)
            if(this.hasOwnProperty(key)) size++;
          return size;
      }
    },
    'length': {
      get: function () {
          let size = 0, key;
          for(key in this)
            if(this.hasOwnProperty(key)) size++;
          return size;
      }
    },
    'isObject': {
      get: function () {
        return !(this instanceof Nullable) && typeof this === 'object' && this !== null;
      }
    },
    'isString': {
      get: function() {
        return (typeof this === 'string' || this instanceof String);
      }
    },
    'isJson': {
      get: function() {
        try {
          JSON.parse(this);
        } catch (e) {
          return false;
        }
        return true;
      }
    },
    'isFunction': {
      get: function() {
        return !!(this && this.constructor && this.call && this.apply);
      }
    },
    'isAsync': {
      get: function() {
        return this[Symbol.toStringTag] === 'AsyncFunction';
      }
    },
    'isNumber': {
      get: function() {
        if(typeof this === 'number' || this.isString && this.match(/^[-+]?[0-9]*[,\.]?[0-9]*$/g))
          return true;

        return false;
      }
    },
    'isElement': {
      get: function() {
        return (
          typeof HTMLElement === "object" ? this instanceof HTMLElement :
          this && typeof this === "object" && this !== null && this.nodeType === 1 && typeof this.nodeName === "string"
        );
      }
    },
    'toNumber': {
      get: function() {
        let self = Number(this) ? this : this.replace(/,/g, '.').replace(/^\./g, '0.').replace(/\.$/g, '.00');

        if(self.match(/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/)) {
          return parseFloat(self);
        } else {
          return parseInt(self);
        }
        return Number(self);
      }
    },
    'watch': {
      enumerable: false,
      configurable: true,
      writable: false,
      value: function(prop, handler) {
        let old, cur, getter, setter;
        old = this[prop];
        cur = old;
        getter = () => cur;
        setter = (val) => {
          old = cur;
          cur = handler.call(this,prop,old,val);
          return cur;
        };

        // can't watch constants
        if(delete this[prop]) {
          Object.defineProperty(this,prop, {
            get: getter,
            set: setter,
            enumerable: true,
            configurable: true
          });
        }
      }
    }
  });

  Object.defineProperties(HTMLElement.prototype, {
    'setEventListener': {
      get: function() {
        return (type, listener, option = false) => {
          let important = false;
          if(option === 'important') {
            option = false;
            important = true;
          }

          if(!this.events)
            this.events = [];

          if(!this.events.includes(listener.toString()) || important) {
            this.events.push(listener.toString());

            if(this.process) {
              listener = listener.bind(that.processes[this.process].script.data);

              this.addEventListener(type, (event) => {
                listener(event);
              }, option);
            }
            else
              this.addEventListener(type, listener, option);
          }
        };
      }
    },
    'toggleClass': {
      get: function() {
        return (classes) => {
          for(let c of classes.replace(/ /, '').split(','))
            this.classList.toggle(c);
            return this;
        };
      }
    },
    'addClass': {
      get: function() {
        return (c) => {
          this.classList.add(c);
          return this;
        };
      }
    },
    'removeClass': {
      get: function() {
        return (c) => {
          this.classList.remove(c);
          return this;
        };
      }
    },
    'hasClass': {
      get: function() {
        return (classes) => {
          let ret = false;
          for(let c of classes.replace(/ /, '').split(',')) {
            ret = this.classList.contains(c);
          }

          return ret;
        };
      }
    },
    'replaceWith': {
      get: function() {
        return (newNode) => {
          let parent = this.parentElement;
          if(newNode.isElement)
            parent.replaceChild(newNode, this);
          else if(newNode.isString) {
            let tmpEle = document.createElement('div');
            tmpEle.innerHTML = newNode;
            parent.replaceChild(tmpEle.firstElementChild, this);
          }

          processElementAdapting(parent);
        };
      }
    },
    'siblings': {
      get: function() {
        return (f) => {
          // Setup siblings array and get the first sibling
          var siblings = [];
          var sibling = this.parentNode.firstChild;

          // Loop through each sibling and push to the array
          while (sibling) {
            if (sibling.nodeType === 1 && sibling !== this) {
              f && f(sibling);
              siblings.push(sibling);
            }

            sibling = sibling.nextSibling
          }

          return siblings;
        };
      }
    },
    'before': {
      get: function() {
        return (newNode) => {
          if(newNode.isElement)
            this.parentNode.insertBefore(newNode, this);
          else if(newNode.isString)
            this.insertAdjacentHTML('beforebegin', newNode);

          processElementAdapting(parent);
        };
      }
    },
    'append': {
      get: function() {
        return (newNode) => {
          if(newNode.isElement)
            this.appendChild(newNode);
          else if(newNode.isString)
            this.insertAdjacentHTML('beforeend', newNode);

          processElementAdapting(this);
        };
      }
    },
    'html': {
      get: function() {
        return (newNode) => {
          this.innerHTML = '';

          if(newNode.isElement)
            this.append(newNode);
          else if(newNode.isString)
            this.insertAdjacentHTML('beforeend', newNode);

          processElementAdapting(this);
        };
      }
    },
    'getNodeList': {
      get: function() {
        return (type = 'all', filter) => {
          let treeWalker, nodeType, nodeFilter, nodeList = [];

          if(type.isFunction) {
            filter = type;
            type = 'all';
          }

          switch(type) {
            case 'element':
              nodeType = NodeFilter.SHOW_ELEMENT;
            break;
            case 'text':
              nodeType = NodeFilter.SHOW_TEXT;
            break;
            case 'attribute':
              nodeType = NodeFilter.SHOW_ATTRIBUTE;
            break;
            case 'comment':
              nodeType = NodeFilter.SHOW_COMMENT;
            break;
            case 'all':
              nodeType = NodeFilter.SHOW_ALL;
            break;
          }

          nodeFilter = node => {
            if(filter ? filter(node) : true)
              return NodeFilter.FILTER_ACCEPT;
            else
              return NodeFilter.FILTER_SKIP;
          };

          treeWalker = document.createTreeWalker(this, nodeType, nodeFilter, false);

          while(treeWalker.nextNode()) {
            nodeList.push(treeWalker.currentNode);
          }

          return nodeList;
        };
      }
    }
  });

  let processElementAdapting = (element) => {
    if(element.process) {
      let nodeList = element.getNodeList('element', node => {
        return !node.process;
      });

      for(let node of nodeList) {
        node.process = element.process;
      }
    }
  };

  return undefined;
}
;
let Caller = function caller(that, config) {
  let responseStore = {};

  let getResponse = async (url, obj) => { // find response in the responseStore
    let responseUrl = url.replace(/^\/|\.\//, location.origin+'/');

    if(!responseUrl.includes(location.origin) && !/^((http|https|ftp):\/\/)/.test(responseUrl)) {
      responseUrl = `${location.origin}/${responseUrl}`;
    }

    if(!responseUrl.match(new RegExp(location.origin, 'ig'))) {
      obj.credentials = 'include';
    } else {
      obj.credentials = 'same-origin';
    }

    return responseStore[responseUrl] || await fetch(url, obj);
  };

  let request = async (obj = {}, tryCounter = 0) => {
    let url = obj.isString ? obj : obj.url, response,
        requestObj = {
          method: obj.method || 'GET',
          headers: obj.headers || {},
          mode: 'cors',
          cache: 'default'
        };

    tryCounter++;

    if(requestObj.method === 'POST') { // its a post request
      requestObj.headers['Content-Type'] = obj.data.isObject ? 'application/json;charset=utf-8' : 'text/plain;charset=utf-8';
      requestObj['body'] = JSON.stringify(obj.data);
    }

    response = await getResponse(url, requestObj);

    if(response.ok) {
      !response.content && (response.content = await response.text());

      if(obj && obj.isObject && obj.save && !responseStore[response.url]) responseStore[response.url] = response;
      // TODO: implement the POST correct
    } else {
      if(tryCounter <= config.maxRequestTry) {
        console.warn(`Caller: "${url}" request failed, try ${tryCounter}`);
        //obj = obj ? obj : url;
        return await request(obj, tryCounter);
      } else {
        console.error(`Caller[${url}]:  ${response.status}`);
      }
    }

    return response;
  };

  let get = async (obj) => {
    let response = await request(obj);
    return response;
  };

  let post = async (obj) => {
    obj.method = 'POST';
    let response = await request(obj);
    return response;
  };

  // TODO:
  // create a preeloading caller process
  // create a combined file loading multiple files in one file

  return {
    get,
    post
  };
}
;
let NodePlayer = function nodePlayer(that) {
  // let customElements = {};

  let createProps = (obj, id, attr) => {
    attr = attr.isString ? attr.split('|') : attr.value.split('|');
    !obj.childProps && (obj.childProps = {});
    !obj.childProps[id] && (obj.childProps[id] = {});

    if(attr[1].isNumber) {
      obj.childProps[id][attr[0]] = attr[1].toNumber;
    } else if(attr[1].match(/[0-1a-zA-z\.\[\]]+/g).length === 1 && eval(`obj.script.data.${attr[1]}`)) {
      if(obj.parent && obj.parent.childProps[obj.id]?.[attr[0]]) { // prop delegate from parent
        obj.childProps[id][attr[0]] = obj.parent.childProps[obj.id][attr[0]];
      } else { // prop delegate from current
        obj.childProps[id][attr[0]] = {
          val: eval(`obj.script.data.${attr[1]}`),
          name: attr[1],
          process: obj
        }
      }
    } else { // prop is a string
      obj.childProps[id][attr[0]] = attr[1];
    }
  };

  let _if = (obj, ele) => {
    let check, ifAttr = ele.getAttribute('if');

    if(ifAttr == 'true' || ifAttr == '1') check = true;
    else if(ifAttr == 'false' || ifAttr == '0') check = false;
    else {
      if(ifAttr.match(/^i(\.| |\[)/g)) {
        check = eval(ifAttr.replace(/^i\./, 'obj.').replace(/^i /, 'obj ').replace(/^i\[/, 'obj['));
      } else {
        check = eval(`obj.script.data.${ifAttr}`);
      }
    }

    if(check) { // true
      ele.removeAttribute('if');
      return false;
    }

    // false
    ele.remove();
    return true;
  };

  let _loop = (obj, ele) => {
    let output = '';
    let dataName = ele.getAttribute('loop');

    let data = null;
    if(dataName.isString && !dataName.isNumber && !dataName.includes('...')) {
      data = eval(`obj.script.data['${dataName.split('.').join(`']['`)}']`);
    }

    let ifAttribute = ele.hasAttribute('if') ?  `i.${ele.getAttribute('if')}` : true;

    ele.removeAttribute('loop');
    ele.removeAttribute('if');

    let counter = 0;
    let preContainer = document.createElement('div');

    let setLoopData = (d) => {
      let i = d;

      if(eval(ifAttribute)) { // output for looped in an object
        preContainer.innerHTML = ele.outerHTML.replace(/process="[0-9]{8}/g, (m, g) => {
          if(ele.localName === 'component')
            createProps(obj, `${m}-${counter}`.replace('process="', ''), `${dataName}|${dataName}[${counter}]`);
          return `${m}-${counter}`;
        });

       // check the if´s
       let nodeList = preContainer.querySelectorAll('[if]');
       for(let el of nodeList) {
         _if(i, el);
       }

       // replace n or calculation
       preContainer.innerHTML = preContainer.innerHTML.replace(/\{\{(([\d.]+|n?)[\+\-\*\/\% ]+([\d.]+|n)|n)\}\}/g, (m, c) => {
         return eval(c.replace('n', d.isNumber ? d : counter));
       });

       // replace k with object key
       preContainer.innerHTML = preContainer.innerHTML.replace(/\{\{k\}\}/g, m => {
         return Object.keys(data).find(key => data[key] === d);
       });

       // replace data
       preContainer.innerHTML = preContainer.innerHTML.replace(/\{\{([[a-zA-Z0-9\-\.\[\]]+)\}\}/g, (m, c) => {
         return eval(c);
       });

       output += preContainer.innerHTML;
      }

      counter++;
    };

    // create the output with replaced data
    if(data) {
      if(Array.isArray(data)) { // loop array
        for(let i of data) {
          setLoopData(i);
        }
      } else if(data.isObject) { // loop object
        for(let i in data) {
          setLoopData(data[i]);
        }
      }
    } else {
      if(dataName.isNumber) { // loop by number
        for(let n = 0, max = Number(dataName); n < max; n++) {
          setLoopData(n);
        }
      } else if(dataName.isString) {
        let parts = dataName.match(/(\d*)\.{3}(\d*)(\|\d*)?/);
        let start = parts[1].toNumber;
        let end = parts[2].toNumber;
        let step = parts[3].slice(1).toNumber;

        if(start < end)
          for(start, end; start <= end; start+=step) setLoopData(start);
        else
          for(start, end; start >= end; start-=step) setLoopData(start);
      } else {
        ele.remove();
      }
    }

    if(output !== '') {
      ele.insertAdjacentHTML('afterend', output);
      ele.remove();
      return true;
    }

    return false;
  };

  let _ref = (obj, ele, attr) => {
    obj.refs[attr.value] = ele;
    ele.removeAttribute('ref');
  };

  let loopAttributNodes = async (obj, ele) => { // loop all atributes of target element
    let isChanged = false;

    if(ele.attributes.length) {
      for(let attr of [...ele.attributes]) {
        attr.value = await that.replacer.interpreting(attr.value, obj.script.data);
        switch (attr.name) {
          case (attr.name.match(/^:prop/) || {}).input:
            createProps(obj, ele.getAttribute('process'), attr);
            break;
          case 'if':
            isChanged = _if(obj, ele);
            break;
          case 'loop':
            isChanged = _loop(obj, ele);
            break;
          case 'ref':
            _ref(obj, ele, attr);
            break;
        }

        if(isChanged) break;
      }
    }

    return isChanged;
  };

  let loopElementNodes = async (obj) => { // loop al element nodes
    let isChanged = false;

    for(let node of obj.template.getNodeList('element')) {
      // loop all attributes
      if(isChanged = await loopAttributNodes(obj, node)) break;
       if(node.localName === 'component' && !node.rendering) {
        if(node.hasAttribute('continue')) {
          if(node.getAttribute('continue') !== 'true') {
            node.append(that.getLoader());
            node.setAttribute('continue', 'true');
            setTimeout(async function() {
              await that.renderer.digest({
                id: (node.getAttribute('process').match(/^[0-9]+$/) ? node.getAttribute('process').toNumber : node.getAttribute('process')) || null,
                url: node.getAttribute('name'),
                ref: node,
                continue: true,
                type: node.localName,
                parent: obj
              });
            }, 0);
          }
        } else {
          await that.renderer.digest({
            id: (node.getAttribute('process').match(/^[0-9]+$/) ? node.getAttribute('process').toNumber : node.getAttribute('process')) || null,
            url: node.getAttribute('name'),
            ref: node,
            type: node.localName,
            parent: obj
          });
        }
      } else if(!node.process) {
         node.process = obj.id; // add the id from delegated process as a value of html Element
      }
    }

    isChanged && await loopElementNodes(obj);
  };

  return {
    loopElementNodes
  }
}
;
let Renderer = function renderer(that) {
  let extraktScript = async (source, scopedData) => {
    source = source.match(/<script>([^⹇]*)<\/script>/);
    source = !source ? 'data: {}' : source[1].trim();
    // create script object
    let script = eval(`(async function(${scopedData.args}) {
      let renderer, extraktScript, source, scopedData;

      return { ${source} };
    } );`);

    return await script();
  };

  let _renderer = that => {
    let getScopedData = (obj) => {
      let scope = that.scope;
      let args = '';

      scope['refs'] = obj.refs;

      for(let i in scope) {
        args += `, ${i} = scopedData.${i}`;
      }

      scope['args'] = args.replace(/^, /, '');

      return scope;
    };

    let getReference = (selector) => { // no selector
      if(!selector) {
        selector = that.settings.general.appContainer;
      }

      if(selector.isString) { // selector is string
        selector = document.querySelector(selector);
      }

      if(selector.isElement) { // element exists
        return selector;
      }

       // element not exists
      console.error(`Renderer: can´t find the referenced Element in DOM!`);
      return null;
    };

    let getSource = async (obj) => {
      if(obj.url) {
        if(obj.type === 'component') {
          obj.url = obj.url.includes('/') ? `${obj.url}` : `./components/${obj.url}`
        }

        let response = await that.caller.get({ url: obj.url, save: true });

        if(response.ok) {
          return response.content;
        } else {
          console.error(`${obj.type} "${obj.url}" not found! - ${response.status}`);
        }
      } else {
        if(obj.type === 'page')
          console.error(`${obj.type} have no Route!`);
        else {
          console.error(`${obj.type} "${obj.ref.getAttribute('name')}" have no Route!`);
        }
      }
    };

    let modifySource = (source) => {
      for(let i of that.componentProvider.names()) {
         source = source.replace(new RegExp(`<(${i})|<\/(${i})`, 'g'), (m, g) => {
           if(m.match(/^<\//)) {
             return `</component`;
           }
           return `<component name="${g}"`;
         });
      }

      for(let i of source.match(/process=("|')([\d\-\w]+)("|')/g) || []) {
        that.getId(i.match(/("|')([^⹇]*)("|')/)[2]);
      }

      return source.replace(/<[^/]?\s*(component)\s*[^>]*>/g, (m, g) => {
        let propCounter = -1;
        return m.replace(g, `${g} process="${that.getId()}"`)
                .replace(/:([\w]{1}[\w\d-]*)="/g, (m, g) => {
                  propCounter++;
                  return `:prop-${propCounter}="${g}|`;
                });
      })
      .replace(/\/\*[\s\S]*?\*\/|([^\\:]|^)\/\/.*$/gm, '') // remove js comments
      .replace(/<!--(.*?)-->/gm, '') // remove html comments
      .replace(/(?!<\")\/\*[^\*]+\*\/(?!\")/gm, ''); // remove css comments
    };

    let extraktTemplate = async (obj) => {
      let tplString = obj.source.match(/<template>([^⹇]*)<\/template>/) ? obj.source.match(/<template>([^⹇]*)<\/template>/)[1].trim() : null;
      let template = document.createElement('div');
      if(tplString === null) {
        console.error(`Source was not found "${obj.url}"`);
      } else {
        // runn the item injecting
        if(obj.isChildren && obj.parent.childItems && obj.parent.childItems[obj.id]) {
          tplString = tplString.replace(/::\[([0-9]+)\]::/gm, (m, g) => {
            return obj.parent.childItems[obj.id][g];
          });
        }

        template.innerHTML = await that.replacer.interpreting(tplString);
      }
      return template;
    };

    let extraktStyle = async (source, id) => {
      let style = source.match(/<style[^⹇]*<\/style>/gi);
      return style ? `<style process="${id.toString().split('-')[0]}">${await that.replacer.interpreting(style[0].replace(/(<style[^<>]*(scope="([a-zA-Z]+[\w\d-]*)")[^<>]*|<style[^<>]*)>([^<]*)<\/style>/g, (m, c0, scopeAttribute, scope, css) => { return scope ? css.trim().replace(/[^}]*{/g, m => { return ` .${scope} ${m.trim()}`; }) : css; }))}</style>` : ''; };

    let buildComputedData = (script) => {
      for(let i in script.comp?.isObject ? script.comp : {}) {
        script.data[i] = script.comp[i].isFunction
                       ? script.comp[i].call(script.data)
                       : script.comp[i];
      }

      script.comp = undefined;
      delete script.comp;
    };

    let bindStepMethods = (script) => { // bind data to process step methods
      if(script.pre && script.pre.isFunction)
        script.pre = script.pre.bind(script.data);

      if(script.created && script.created.isFunction)
        script.created = script.created.bind(script.data);

      if(script.merged && script.merged.isFunction)
        script.merged = script.merged.bind(script.data);
    };

    let bindScriptPropsMethods = (script) => {
      // bind data to script methods
      for(let i in script.props?.isObject ? script.props : {}) {
        if(script.props[i].isFunction) {
          script.props[i] = script.props[i].bind(script.data);
        }
      }
    };

    let bindScriptMethods = (script) => {
      // bind data to script methods
      for(let i in script.methods?.isObject ? script.methods : {}) {
        if(script.methods[i].isFunction) {
          script.data[i] = script.methods[i].bind(script.data);
        }
      }

      script.methods = undefined;
      delete script.methods;
    };

    let buildEventListeners = async (obj) => {
      let selectors, selector, nodes, node, e;

      for(e of obj.script.events) {
        selectors = e[0].split(',');
        nodes = [];

        for(selector of selectors) {
          selector.replace(/^ *(refs\.[a-zA-z]{1}[\w-]*)?([\w-\.\[\]="':# ]+)?$/, (m, c1, c2) => {
            if(c1 && !c2) { // only ref selecting
              obj.refs && nodes.push(eval(`obj.${c1}`));
            } else if(!c1 && c2) { // only normal selector selecting
              nodes = nodes.concat([...obj.template.querySelectorAll(c2)]);
            } else if(c1 && c2) { // ref and normal selector selecting
              nodes = nodes.concat([...eval(`obj.${c1}`).querySelectorAll(c2)]);
            }
          });
        }

        for(node of nodes) {
          node.addEventListener(e[1], e[2].bind(obj.script.data));
        }
      }
    };

    let bindWatchingMethods = (script) => {
      // bind data to script methods
      for(let i in script.watching?.isObject ? script.watching : {})
        script.watching[i].isFunction && (script.watching[i] = script.watching[i].bind(script.data));
    };

    let injectProps = (obj) => {
      let rootProcess, rootDataName;
      if(obj.isChildren && obj.parent.childProps) { // onli childs
        for(let i in obj.parent.childProps[obj.id]?.isObject ? obj.parent.childProps[obj.id] : {}) {
          if(obj.parent.childProps[obj.id][i].isObject) {
            if(obj.script.props && obj.script.props[i] && obj.script.props[i].isFunction) {
              obj.script.data[i] = obj.script.props[i](obj.parent.childProps[obj.id][i].val);
            } else {
              obj.script.data[i] = obj.parent.childProps[obj.id][i].val;
            }
          } else {
            if(obj.script.props && obj.script.props && obj.script.props[i] && obj.script.props[i].isFunction) {
              obj.script.data[i] = obj.script.props[i](obj.parent.childProps[obj.id][i]);
            } else {
              obj.script.data[i] = obj.parent.childProps[obj.id][i];
            }
          }

          obj.script.data.watch(i, (name, oldVal, newVal) => {
            if(oldVal !== newVal) { // when value is changed
              if(obj.parent.childProps[obj.id][i].isObject) {
                rootProcess = obj.parent.childProps[obj.id][i].process;
                rootDataName = obj.parent.childProps[obj.id][i].name;

                if(rootProcess.script.data[rootDataName] !== newVal) {
                  rootProcess.script.data[rootDataName] = newVal;
                  if(!rootProcess.isRendering && rootProcess.isActive) {
                    digest(rootProcess);
                  }
                }
              } else {
                if(!obj.isRendering && obj.isActive) {
                  digest(obj);
                }
              }
            }
            return newVal;
          });
        }

        for(let i in obj.props?.isObject ? obj.props : {}) {
          if(obj.script.props && obj.script.props[i] && obj.script.props[i].isFunction) {
            obj.script.data[i] = obj.script.props[i](obj.props[i]);
          } else {
            obj.script.data[i] = obj.props[i];
          }
        }
      }
    };

    let setWatcher = (obj) => { // set watchers for data of current process
      for(let i in obj.script.watching?.isObject ? obj.script.watching : {}) {
        if(obj.script.data[i]) {
          obj.script.data.watch(i, (name, oldVal, newVal) => {
            if(oldVal !== newVal) { // when value is changed
              if(!obj.isRendering && obj.isActive) {
                digest(obj);
              }
            }
            return newVal;
          });
        }
      }
    };

    let create = async (obj) => { // create a new Process if it´s not exists
      // setup id
      !obj.id && (obj.id = obj.ref.process || obj.ref.getAttribute('process') || that.getId());

      obj.isChildren = !!obj.parent;
      obj.isPrime = !obj.parent;
      !obj.isActive && (obj.isActive = false);

      that.processes[obj.id] = obj;
      obj.type === 'page' && (that.processes[obj.url] = that.processes[obj.id]);

      obj.isChildren && (obj.parent.children[obj.id] = obj);

      obj.source = await getSource(obj);
      obj.source = modifySource(obj.source);

      await build(obj);
    };

    let reCreate = async (obj) => { // create the Process based on existing Process
      that.processes[obj.id].ref = obj.ref;
      await build(that.processes[obj.id]);
    };

    let build = async (obj) => { // build the process if its not buildet exists
      let oldData;
      obj.refs = {};

      obj.template = await extraktTemplate(obj);
      obj.style = await extraktStyle(obj.source, obj.id);

      if(obj.script) {
        oldData = obj.script.data;
        obj.script = await extraktScript(obj.source, getScopedData(obj));
        obj.script.data = oldData;
      } else {
        obj.script = await extraktScript(obj.source, getScopedData(obj));
      }

      if(!obj.template.innerHTML || !obj.script.length || !obj.style.match(/>([^⹇]*\{+[^⹇]*\}+[^⹇]*)</)) {
        console.warn(`Renderer -> ${obj.url} template have empty parts`);
      }

      bindScriptPropsMethods(obj.script);

      injectProps(obj);

      buildComputedData(obj.script);

      // Binding Data as that
      bindStepMethods(obj.script);
      bindScriptMethods(obj.script);

      // set watchings
      bindWatchingMethods(obj.script);
      setWatcher(obj);

      // runn beforeStack
      if(obj.type === 'page') {
        for(let f of that.beforeStack) {
          f(obj.script.data);
        }
      }

      if(obj.script.pre) {
        if(obj.script.pre.isAsync) {
          await obj.script.pre();
        } else {
          obj.script.pre();
        }
      }

      if(obj.stop === false) {
        await compile(obj);
      } else {
        obj.stop = false;
      }
    };

    let extraktItems = async (obj) => { // extrakt the component delegated items
      let items;
      for(let c of obj.template.querySelectorAll('component')) {
        items = [];
        for(let i of c.querySelectorAll('item')) {
          items.push(i.innerHTML.trim());
        }

        !obj.childItems && (obj.childItems = {})

        if(items.length) {
          obj.childItems[c.getAttribute('process')] = items;
        }

        c.innerHTML = '';
      }
    };

    let compile = async (obj) => { // compile process
      // runn the data replacing process
      await that.replacer.interpreting(obj.template, obj.script.data);

      await extraktItems(obj);

      // handling all Element-Nodes of the Template
      await that.nodePlayer.loopElementNodes(obj);

      if(obj.script.created) {
        if(obj.script.created.isAsync) {
          await obj.script.created();
        } else {
          obj.script.created();
        }
      }

      obj.script.events && await buildEventListeners(obj);

      await merge(obj);
    };

    let merge = async (obj) => { // merge process in ref process
      let oldPage, newRef, reRendered = false;

      // insert Style in Document head
      if(!document.head.querySelector(`style[process="${obj.id.toString().split('-')[0]}"]`)) {
        document.head.append(obj.style);
      }

      if(obj.ref.process === obj.id) {
        reRendered = true;
      }

      // insert in or replace with the Object Reference Element
      switch(obj.type) {
        case 'page':
          oldPage = obj.ref.querySelector('[data-type="page"]');
          obj.template.firstElementChild.dataset.type = 'page';

          if(oldPage) {
            destroy(oldPage.process, (oldPage.process != obj.id));
            oldPage.replaceWith(obj.template.firstElementChild);
          } else {
            obj.ref.append(obj.template.firstElementChild);
          }
          break;

        case 'component':
          newRef = obj.template.firstElementChild;
          if(newRef) {
            newRef.dataset.type = 'component';

            if(obj.ref.localName === 'component' || obj.ref.dataset.type === 'component') {
              obj.ref.replaceWith(newRef);
            } else {
              obj.ref.append(newRef);
            }

            obj.ref = newRef;
          }
          break;
      }

      obj.isActive = true;
      if(obj.script.merged) {
        if(obj.script.merged.isAsync) {
          await obj.script.merged();
        } else {
          obj.script.merged();
        }
      }

      // runn afterStack
      if(obj.type === 'page' || reRendered === true || obj.continue === true) {
        for(let f of that.afterStack) {
          f(obj.script.data);
        }
      }

      obj.isRendering = false;
    };

    let destroy = (id, hard = true) => {
      for(let i in that.processes[id].children) {
        destroy(that.processes[id].children[i].id, hard);
      }

      if(hard) {
        that.processes[that.processes[id].url] && (that.processes[that.processes[id].url].isActive = false);
        that.processes[id].isActive = false;
        delete that.processes[that.processes[id].url];
        delete that.processes[id];

        let style = document.querySelector(`style[process|="${id.toString().split('-')[0]}"]`);
        style && style.parentNode.removeChild(style);
      }
    };

    let stop = (obj) => {
      obj.stop = true;
      obj.isRendering = false;
      obj.isActive = false;
    };

    let digest = async obj => {
      if(obj.isString) obj = { url: obj };

      obj.ref = getReference(obj.ref);
      !obj.url && (obj.url = null);
      !obj.type && (obj.type = 'page');
      !obj.parent && (obj.parent = null);
      !obj.children && (obj.children = {});
      !obj.continue && (obj.continue = false);
      !obj.isRendering && (obj.isRendering = true);
      obj.stop = false;

      that.scope.digest = (o) => { digest(o || obj); };
      that.scope.stop = () => { stop(obj); };

      if(obj.type === 'component') {
        obj.url = that.componentProvider.get(obj.url);
      }

      if(!that.processes[obj.id] && !that.processes[obj.url]) { // create new Process
        await create(obj);
      } else if(obj.type !== 'page') { // component
        if(obj.ref.children.length > 0) {
          if(obj.continue) {
            await create(obj);
          } else {
            await reCreate(obj);
          }
        } else {
          await reCreate(obj);
        }
      } else {
        await reCreate(that.processes[obj.url]);
      }

      return obj.id;
    };

    return {
      digest,
      destroy
    };
  };

  return _renderer(that);
}
;
let Replacer = async function replacer(that) {
  let resourceSet;
  let scrollbarSize = that.getScrollbarSize();

  let requestResource = async url => {
    let response = await that.caller.get({ url, save: true });

    if(response.ok) {
      return /\.json$/.test(url) ? JSON.parse(response.content.replace(/\/\*.*?\*\//gm, '')) : response.content;
    }

    return null;
  };

  let setResource = async () => {
    let resourcesGlobal = await requestResource(`./resources/strings/global.json`);
    let resourcesLocale = await requestResource(`./resources/strings/${that.getLocale()}.json`);
    let resourcesImages = await requestResource(`./resources/images/index.json`);

    if(resourcesGlobal && resourcesLocale && resourcesImages) {
      resourceSet = Object.assign({},resourcesGlobal,
        resourcesLocale,
        resourcesImages);
    }

    resourceSet.scrollbarSize = scrollbarSize;
  };

  let interpreting = (val, data) => {
    if(data !== undefined) {
      return interpreteDataPlaceholder(val, data);
    } else {
      return interpreteStringPlaceholder(val);
    }
  };

  let handlingData = (m, c, data) => {
    let d, parts = c.split('.');

    c = 'data';
    
    for(let part of parts) {
      if(/^[\D][^\W]*$/.test(part)) { // normal string
        c += `?.${part}`;
      } else { // complex string
        if(/^([^\d\s][\w-_]+)\(([^()]*)\)+$/.test(part)) { // its function
          c += `.${part}`;
        } else {
          c += `['${part}']`;
        }
      }
    }

    d = eval(c);

    return d ? d : m;
  };

  let interpreteStringPlaceholder = (val) => {
    let stringResources = resourceSet;
    return stringResources[val] ? stringResources[val] : val.replace(/(\[\[)([a-zA-Z0-9\-\.\(\)|_?!&%$§"`'#:;, ]+)(\]\])/g, (m, c1, c2, c3) => stringResources[c2]);
  };

  let interpreteDataPlaceholder = (val, data) => {
    let pattern = /(\{\{)([a-zA-Z0-9\-\.\(\)|_?!&%$§"`'#:;, ]+)(\}\})/g;

    if(val.isString) {
      return val.replace(pattern, (m, c1, c2, c3) => {
        return handlingData(m, c2, data);
      });
    } else if(val.isElement) {
      for(let node of val.getNodeList('text')) {
        node.data = node.data.replace(pattern, (m, c1, c2, c3) => {
          return handlingData(m, c2, data);
        });
      }
    }
  };

  // init
  await setResource();

  return {
    interpreting
  };
}
;
let Router = function router(that) {
  let popStateEvent = new PopStateEvent('popstate', null);
  let settings = that.settings.router,
      routes = [];

  let add = function(re = '', handler) {
    if(re.isFunction) { handler = re; re = ''; }
    re = that.clearSlashes(re);
    re && (re = new RegExp(re));
    routes.push({ re, handler});
  };

  let getFragment = function() {
    return that.clearSlashes(decodeURI(location.pathname));
  };

  for(let i in settings.routes) {
    add(i, function() { return settings.routes[i].file; });
  }

  let ret = {
    remove: function(param) {
      for(let i = 0, r; i < routes.length, r = routes[i]; i++) {
          if(r.handler === param || r.re.toString() === param.toString()) {
              routes.splice(i, 1);
              return this;
          }
      }
      return this;
    },
    check: async function(fragment = getFragment()) {
      for(let i = 0; i < routes.length; i++) {
        let match = fragment.match(routes[i].re);

        if(match && routes[i].re !== '' || match && fragment === '') {
            let result = routes[i].handler.call({}, match.shift());
            result && await that.renderer.digest(result);
            return this;
        }
      }

      // not found 404-Page
      await that.renderer.digest(settings.errorPages['404']);
      return this;
    },
    listen: async function() {
      let self = this;
      let current = getFragment();
      let fn = async function() {
          if(current !== getFragment()) {
              current = getFragment();
              await self.check(current);
          }
      }

      window.addEventListener('popstate', fn, true);

      return this;
    },
    navigate: function(path = '', replace) {
      if(replace) {
        history.replaceState(null, null, that.clearSlashes(path));
      } else {
        history.pushState(null, null, that.clearSlashes(path));
      }

      dispatchEvent(popStateEvent);
      return this;
    }
  };

  that.scope.navigate = ret.navigate;

  return ret;
}
;
let ComponentProvider = function componentProvider(that) {
  let componentStore = that.settings.components || {};

  let add = (name, path) => componentStore[name] = path;

  let get = name => {
    if(componentStore[name]) {
      return componentStore[name];
    } else {
      console.log(`Component route for ${name} not exists`);
      return name;
    }
  };

  let names = () => Object.keys(componentStore);

  return {
    get,
    add,
    names
  }
}
;
let Store = function store(that) {
  let dataStore;

  /* sessionStorage & localStorage */
  let methods = (storage) => {
    return {
      set: function(key, value) {
          if(!key || !value) return;
          storage.setItem(key, JSON.stringify(value));
      },
      get: function(key) {
          if(!key) return;
          if(this.exists(key))
              return JSON.parse(storage.getItem(key));
          return null;
      },
      add: function(key, value) {
          if(!key || !value) return;
          if(this.exists(key))
              value = JSON.parse(this.get(key))+value;
          storage.setItem(key, JSON.stringify(value));
          return value;
      },
      exists: function(key) { // return boolean of exist
          if(!key) return false;
          if(!storage.getItem(key)) return false;
          return true;
      },
      remove: function(key) {
          if(!key) return;
          if(storage.removeItem(key)) return false;
          return true;
      },
      clear: function() {
          storage.clear();
          if(storage.length) return false;
          return true;
      },
      length: function() {
          return storage.length;
      }
    };
  };

  dataStore = {
    local: methods(localStorage),
    session: methods(sessionStorage),
    global: {},
    data: {}
  };

  that.addBefore(() => { dataStore.data = {}; });
  that.scope.store = dataStore;

  return {
    local: methods(localStorage),
    session: methods(sessionStorage)
  };
}
;
export default async function core(that = {}, module = []) {
  let usedIds = []; // list of used ids

  that.settings = Settings;

// Libs ##################################################################### //
  PrototypeExtensions(that);

// Private-Variables ######################################################## //
  let scrollbarSize;

// CONFIG ################################################################### //
  let config = {
    maxRequestTry: 24
  };

  that.version = {
    core: '1.1.0'
  };

// GLOBAL-OBJECTS ########################################################### //
  that.processes = {};
  that.beforeStack = [];
  that.afterStack = [];
  that.scope = {};
  that.stoped;

// GLOBAL-CORE-METHODS ###################################################### //
  that.getId = staticId => { // generate custom id
    let id = staticId ? staticId : Math.floor((Math.random() * (99999999 - 9999999)) + 9999999);

    if(!staticId && usedIds.includes(id)) {
      return that.getId();
    } else if(staticId && !usedIds.includes(id) || !staticId && !usedIds.includes(id)) {
      usedIds.push(id);
    }

    return id;
  };

  // returned translated strings based on the locale files
  that.t = name => that.replacer.interpreting(`${name}`);

  // remove slasches on bstart and end of string
  that.clearSlashes = path => path.toString().replace(/((\/$)|(^\/)|(^\.\.\/)|(^\.\/))/, '');

  that.urlBuilder = function() {
    let url = '';
    for(let i of arguments) {
      url += !url ? that.clearSlashes(i) : `/${that.clearSlashes(i)}`
    }
    return url;
  };

  that.getScrollbarSize = () => {
    if(scrollbarSize)
      return scrollbarSize;

    let outer, inner;

    // Creating invisible container
    outer = document.createElement('div');
    outer.style.visibility = 'hidden';
    outer.style.overflow = 'scroll'; // forcing scrollbar to appear
    outer.style.msOverflowStyle = 'scrollbar'; // needed for WinJS apps
    document.body.appendChild(outer);

    // Creating inner element and placing it in the container
    inner = document.createElement('div');
    outer.appendChild(inner);

    // Calculating difference between container's full width and the child width
    scrollbarSize = (outer.offsetWidth - inner.offsetWidth);

    // Removing temporary elements from the DOM
    outer.parentNode.removeChild(outer);

    return scrollbarSize;
  };

  that.setLocale = newLocale => {
    let storedLocale = that.store.local.get('locale'),
        currentLocale = document.documentElement.getAttribute('lang');

    if(newLocale) {
      that.store.local.set('locale', newLocale);
      document.documentElement.setAttribute('lang', newLocale);
    } else if(storedLocale) {
      document.documentElement.setAttribute('lang', storedLocale);
    } else {
      that.store.local.set('locale', currentLocale);
    }

    if(document.documentElement.getAttribute('lang') !== currentLocale) {
      navigate(location.href, true);
    }
  };

  that.getLocale = () => {
    return that.store.local.get('locale') || that.settings.general.locale;
  };

  that.addStyle = async (path) => {
    let css = await that.caller.get(path);
    document.head.querySelector('script').before(`<style>${that.replacer.interpreting(css.content)}</style>`);
  };

  that.addBefore = (f) => {
    that.beforeStack.push(f);
  };

  that.addAfter = (f) => {
    that.afterStack.push(f);
  };

  that.getLoader = () => {
    return `<div class="spinner">
                     <div class="bounce1"></div>
                     <div class="bounce2"></div>
                     <div class="bounce3"></div>
                   </div>`;
  };

  // add to the scope
  that.scope.setLocale = that.setLocale;
  that.scope.getLocale = that.getLocale;
  that.scope.getLoader = that.getLoader;
  that.scope.t = that.t;

// INIT ##################################################################### //
  // create App Container
  that.appContainer = document.querySelector('#App');
  if(!that.appContainer) {
    document.body.append(`<div id="App"></div>`);
    that.appContainer = document.querySelector('#App');
  }

  // add JavaScript
  let loadScriptsAfter = () => {
    for(let src of that.settings.jsFiles) {
      let script = document.createElement("script");
      script.defer = true;
      script.src = src;
      document.head.append(script);
    }
  };

  if(window.addEventListener) {
    window.addEventListener("load", loadScriptsAfter);
  } else if(window.attachEvent) {
    window.attachEvent("onload", loadScriptsAfter);
  } else {
    window.onload = loadScriptsAfter;
  }

  that.caller = Caller(that, config);
  that.store = Store(that);
  that.router = Router(that);
  await that.router.listen();

  that.setLocale();

  that.replacer = await Replacer(that);
  that.nodePlayer = NodePlayer(that);
  that.componentProvider = ComponentProvider(that);
  that.renderer = Renderer(that);

  for(let cssFile of that.settings.cssFiles) {
    await that.addStyle(cssFile);
  }

  // load the modules
  for(let i of module) {
    i.isAsync ? await i(that) : i(that);
  }


  await that.router.check();
}
;